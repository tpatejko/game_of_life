extern crate rand;

use rand::{
    distributions::{Distribution, Standard},
    Rng,
};

#[derive(Copy, Clone)]
pub enum State {
    Live,
    Dead,
}

impl Distribution<State> for Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> State {
        match rng.gen_range(0, 2) {
            0 => State::Live,
            _ => State::Dead,
        }
    }
}

pub struct Grid {
    pub height: usize,
    pub width: usize,
    cells: Vec<State>,
}

impl Grid {
    pub fn new(height: usize, width: usize) -> Grid {
        let mut cells = Vec::<State>::new();

        for _ in 0..width * height {
            cells.push(State::Dead);
        }

        Grid {
            height: height,
            width: width,
            cells: cells,
        }
    }

    pub fn index(&self, x: usize, y: usize) -> usize {
        return y * self.width + x;
    }

    pub fn index_mut(&mut self, x: usize, y: usize) -> usize {
        return y * self.width + x;
    }

    pub fn state(&self, x: usize, y: usize) -> State {
        self.cells[self.index(x, y)]
    }

    pub fn assign_state(&mut self, x: usize, y: usize, state: State) {
        let idx = self.index_mut(x, y);
        self.cells[idx] = state;
    }
}

fn calculate_live_neighbours(neighbours: &Vec<State>) -> usize {
    let mut live = 0;

    for cs in neighbours {
        live += match cs {
            State::Live => 1,
            State::Dead => 0,
        }
    }

    live
}

fn new_state(state: State, neighbours: &Vec<State>) -> State {
    let count = calculate_live_neighbours(neighbours);

    let new_state = match (count, state) {
        (c, State::Live) if c < 2 => State::Dead,
        (c, State::Live) if c == 2 || c == 3 => State::Live,
        (c, State::Live) if c > 3 => State::Dead,
        (c, State::Dead) if c == 3 => State::Live,
        (_, state) => state,
    };

    new_state
}

pub fn form_new_generation(grid: &Grid) -> Grid {
    let mut new_cells: Vec<State> = Vec::new();
    let height = grid.height;
    let width = grid.width;

    {
        // upper-left corner
        new_cells.push(new_state(
            grid.state(0, 0),
            &vec![
                /* 0, 0 */ grid.state(1, 0),
                grid.state(0, 1),
                grid.state(1, 1),
            ],
        ));
    }

    {
        // upper row
        for w in 1..grid.width - 1 {
            new_cells.push(new_state(
                grid.state(w, 0),
                &vec![
                    grid.state(w - 1, 0),
                    /* (w, 0) */ grid.state(w + 1, 0),
                    grid.state(w - 1, 1),
                    grid.state(w, 1),
                    grid.state(w + 1, 1),
                ],
            ));
        }
    }

    {
        // upper-right corner
        new_cells.push(new_state(
            grid.state(grid.width - 1, 0),
            &vec![
                grid.state(width - 2, 0), /* width-1, 0 */
                grid.state(width - 2, 1),
                grid.state(width - 1, 1),
            ],
        ));
    }

    {
        for h in 1..grid.height - 1 {
            new_cells.push(new_state(
                grid.state(0, h),
                &vec![
                    grid.state(0, h - 1),
                    grid.state(1, h - 1),
                    /* 0, h */ grid.state(1, h),
                    grid.state(0, h + 1),
                    grid.state(1, h + 1),
                ],
            ));

            for w in 1..grid.width - 1 {
                new_cells.push(new_state(
                    grid.state(w, h),
                    &vec![
                        grid.state(w - 1, h - 1),
                        grid.state(w, h - 1),
                        grid.state(w + 1, h - 1),
                        grid.state(w - 1, h),
                        /* w, h */ grid.state(w + 1, h),
                        grid.state(w - 1, h + 1),
                        grid.state(w, h + 1),
                        grid.state(w + 1, h + 1),
                    ],
                ));
            }

            new_cells.push(new_state(
                grid.state(width - 1, h),
                &vec![
                    grid.state(width - 2, h - 1),
                    grid.state(width - 1, h - 1),
                    grid.state(width - 2, h), /* width-1, h */
                    grid.state(width - 2, h + 1),
                    grid.state(width - 1, h + 1),
                ],
            ));
        }
    }

    {
        // lower-left corner
        new_cells.push(new_state(
            grid.state(0, height - 1),
            &vec![
                grid.state(0, height - 2),
                grid.state(1, height - 2),
                /* 0, height-1 */ grid.state(1, height - 1),
            ],
        ));
    }

    {
        // last row
        for w in 1..grid.width - 1 {
            new_cells.push(new_state(
                grid.state(w, height - 1),
                &vec![
                    grid.state(w - 1, height - 2),
                    grid.state(w - 1, height - 2),
                    grid.state(w + 1, height - 2),
                    grid.state(w - 1, height - 1),
                    /* w, height-1 */ grid.state(w + 1, height - 1),
                ],
            ));
        }
    }

    {
        // lower-right corner
        new_cells.push(new_state(
            grid.state(width - 1, height - 1),
            &vec![
                grid.state(width - 2, height - 2),
                grid.state(width - 2, height - 1),
                grid.state(width - 1, height - 1), /* width-1, height-1 */
            ],
        ));
    }

    Grid {
        height: grid.height,
        width: grid.width,
        cells: new_cells,
    }
}

pub fn random_grid(height: usize, width: usize) -> Grid {
    let mut grid = Grid::new(height, width);
    let mut rng = rand::thread_rng();

    for h in 0..height {
        for w in 0..width {
            let s: State = rng.gen();
            grid.assign_state(h, w, s);
        }
    }

    grid
}

#[test]
fn test_new_grid() {
    let g = Grid::new(2, 2);

    assert!(matches!(g.state(0, 0), State::Dead));
    assert!(matches!(g.state(0, 1), State::Dead));
    assert!(matches!(g.state(1, 0), State::Dead));
    assert!(matches!(g.state(1, 1), State::Dead));
}

#[test]
fn test_assign_state() {
    let mut g = Grid::new(2, 2);

    g.assign_state(0, 0, State::Live);
    g.assign_state(0, 1, State::Dead);
    g.assign_state(1, 0, State::Live);
    g.assign_state(1, 1, State::Dead);

    assert!(matches!(g.state(0, 0), State::Live));
    assert!(matches!(g.state(0, 1), State::Dead));
    assert!(matches!(g.state(1, 0), State::Live));
    assert!(matches!(g.state(1, 1), State::Dead));
}

#[test]
fn test_single_generation() {
    let mut g1 = Grid::new(2, 2);

    g1.assign_state(0, 0, State::Dead);
    g1.assign_state(0, 1, State::Live);
    g1.assign_state(1, 0, State::Live);
    g1.assign_state(1, 1, State::Live);

    let g2 = form_new_generation(&g1);

    assert!(matches!(g2.state(0, 0), State::Live));
    assert!(matches!(g2.state(0, 1), State::Live));
    assert!(matches!(g2.state(1, 0), State::Live));
    assert!(matches!(g2.state(1, 1), State::Live));
}

#[test]
fn test_single_generation_tub() {
    let mut g1 = Grid::new(3, 3);

    g1.assign_state(0, 0, State::Dead);
    g1.assign_state(0, 1, State::Live);
    g1.assign_state(0, 2, State::Dead);
    g1.assign_state(1, 0, State::Live);
    g1.assign_state(1, 1, State::Dead);
    g1.assign_state(1, 2, State::Live);
    g1.assign_state(2, 0, State::Dead);
    g1.assign_state(2, 1, State::Live);
    g1.assign_state(2, 2, State::Dead);

    let g2 = form_new_generation(&g1);

    assert!(matches!(g2.state(0, 0), State::Dead));
    assert!(matches!(g2.state(0, 1), State::Live));
    assert!(matches!(g2.state(0, 2), State::Dead));
    assert!(matches!(g2.state(1, 0), State::Live));
    assert!(matches!(g2.state(1, 1), State::Dead));
    assert!(matches!(g2.state(1, 2), State::Live));
    assert!(matches!(g2.state(2, 0), State::Dead));
    assert!(matches!(g2.state(2, 1), State::Live));
    assert!(matches!(g2.state(2, 2), State::Dead));
}

#[test]
fn test_single_generation_blinker() {
    let mut g1 = Grid::new(3, 3);

    g1.assign_state(0, 0, State::Dead);
    g1.assign_state(0, 1, State::Live);
    g1.assign_state(0, 2, State::Dead);
    g1.assign_state(1, 0, State::Dead);
    g1.assign_state(1, 1, State::Live);
    g1.assign_state(1, 2, State::Dead);
    g1.assign_state(2, 0, State::Dead);
    g1.assign_state(2, 1, State::Live);
    g1.assign_state(2, 2, State::Dead);

    let g2 = form_new_generation(&g1);

    assert!(matches!(g2.state(0, 0), State::Dead));
    assert!(matches!(g2.state(0, 1), State::Dead));
    assert!(matches!(g2.state(0, 2), State::Dead));
    assert!(matches!(g2.state(1, 0), State::Live));
    assert!(matches!(g2.state(1, 1), State::Live));
    assert!(matches!(g2.state(1, 2), State::Live));
    assert!(matches!(g2.state(2, 0), State::Dead));
    assert!(matches!(g2.state(2, 1), State::Dead));
    assert!(matches!(g2.state(2, 2), State::Dead));

    let g3 = form_new_generation(&g2);

    assert!(matches!(g3.state(0, 0), State::Dead));
    assert!(matches!(g3.state(0, 1), State::Live));
    assert!(matches!(g3.state(0, 2), State::Dead));
    assert!(matches!(g3.state(1, 0), State::Dead));
    assert!(matches!(g3.state(1, 1), State::Live));
    assert!(matches!(g3.state(1, 2), State::Dead));
    assert!(matches!(g3.state(2, 0), State::Dead));
    assert!(matches!(g3.state(2, 1), State::Live));
    assert!(matches!(g3.state(2, 2), State::Dead));
}
