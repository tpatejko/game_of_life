extern crate nannou;

use nannou::prelude::*;
pub mod life;

struct Model {
    cell_w: usize,
    cell_h: usize,
    grid: life::Grid,
}

fn main() {
    nannou::app(model).update(update).run();
}

fn model(app: &App) -> Model {
    let _window = app.new_window().size(800, 600).view(view).build().unwrap();
    let w_cells = 100;
    let h_cells = 100;

    let max_window_w = 800;
    let max_window_h = 600;

    let cell_w = max_window_w / w_cells;
    let cell_h = max_window_h / h_cells;

    Model {
        cell_h: cell_h,
        cell_w: cell_w,
        grid: life::random_grid(h_cells, w_cells),
    }
}

fn update(_app: &App, model: &mut Model, _update: Update) {
    let new_grid = life::form_new_generation(&model.grid);
    model.grid = new_grid;
}

fn view(app: &App, model: &Model, frame: Frame) {
    // 100-step and 10-step grids.
    let draw = app.draw();
    let win = app.window_rect();//main_window().rect();
    draw.background().color(BLACK);

    draw_grid(
        &draw,
        &win,
        &model.grid,
        model.cell_w as f32,
        model.cell_h as f32,
    );

    draw.to_frame(app, &frame).unwrap();
}

fn draw_grid(draw: &Draw, win: &Rect, grid: &life::Grid, cell_w: f32, cell_h: f32) {
    let left = win.left();
    let bottom = win.bottom();

    for h in 0..grid.height {
        for w in 0..grid.width {
            let color = match grid.state(h, w) {
                life::State::Dead => BLACK,
                life::State::Live => WHITE,
            };
            draw.rect()
                .color(color)
                .w(cell_w)
                .h(cell_h)
                .x_y(left + w as f32 * cell_w, bottom + h as f32 * cell_h);
        }
    }
}
